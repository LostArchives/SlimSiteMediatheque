let entityInstance = null;

class entityJs {

	constructor() {
        if (!entityInstance) {
            entityInstance = this;
        }
        return entityInstance;
    }

	getEditForm(id_entity) {
        let myform = "";
		let checked = '';
        let available = $("#entity_available-"+id_entity).text();
		if (available=='Oui') {
			checked = 'checked';
			available = 1;
		}
		else {
			checked = '';
			available = 0;
		}
        let nb_times_borrowed = $("#entity_timesBorrowed-"+id_entity).text();
		let item_status_rate = $("#entity_statusRate-"+id_entity).text().split('(')[1].replace(')','');
		
        myform += '<div class="form-group" style="display:inline-block;float:left;margin-right:10px">';
        myform += '<label>ID Entité (N° Série)</label>';
        myform += "<input class='form-control' id='new_entity_id-"+id_entity+"' type='text' value='"+id_entity+"' disabled>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:10px">';
        myform += '<label>Nombre de fois emprunté</label>';
        myform += "<input class='form-control' id='new_entity_borrowed-"+id_entity+"' type='number' min='0'  value='"+nb_times_borrowed+"'>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:10px;">';
        myform += '<label>Etat de l\'objet (0 - 10)</label>';
        myform += "<input class='form-control' id='new_entity_rate-"+id_entity+"' type='number' min='0' max='10'  value='"+item_status_rate+"'>";
        myform += '</div>';
		myform += '<div class="form-group" style="display:inline-block;margin-right:10px">';
        myform += '<label>Disponible</label>';
        myform += "<input class='form-control' id='new_entity_available-"+id_entity+"' type='checkbox' "+checked+" >";
        myform += '</div>';
        return myform;
		
    }
	
	getAddForm(id_book) {
		let myform = "";
		let book_name = $("#book_title-"+id_book).text().split(': ')[1];
		let book_author = $("#book_author-"+id_book).text().split(': ')[1];
		let book_category = $("#book_category-"+id_book).text().split(': ')[1];
		
		myform += '<div class="form-group" style="display:inline-block;float:left;margin-right:10px">';
        myform += '<label>Titre du livre</label>';
        myform += "<input class='form-control' id='new_entity_book_name-00' type='text' value='"+this.escapeHtml(book_name)+"' disabled>";
        myform += '</div>';
		myform += '<div class="form-group" style="display:inline-block;margin-right:10px">';
        myform += '<label>Auteur du livre</label>';
        myform += "<input class='form-control' id='new_entity_book_author-00' type='text' value='"+this.escapeHtml(book_author)+"' disabled>";
        myform += '</div>';
		myform += '<div class="form-group" style="display:inline-block;margin-right:10px">';
        myform += '<label>Catégorie Livre</label>';
        myform += "<input class='form-control' id='new_entity_book_category-00' type='text' value='"+this.escapeHtml(book_category)+"' disabled>";
        myform += '</div>';
		myform += '<div class="form-group" style="display:inline-block;margin-right:10px">';
        myform += '<label>ID Entité (N° Série)</label>';
        myform += "<input class='form-control' id='new_entity_id-00' type='text' value=''>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:10px">';
        myform += '<label>Nombre de fois emrpunté</label>';
        myform += "<input class='form-control' id='new_entity_borrowed-00' type='number' min='0'  value='0'>";
        myform += '</div>';
        myform += '<div class="form-group" style="display:inline-block;margin-right:15px">';
        myform += '<label>Etat de l\'objet (0 - 10)</label>';
        myform += "<input class='form-control' id='new_entity_rate-00' type='number' min='0' max='10'  value='0'>";
        myform += '</div><br/>';
		myform += '<div class="form-group" style="display:inline-block">';
        myform += '<label>Disponible</label>';
        myform += "<input class='form-control' id='new_entity_available-00' type='checkbox' >";
        myform += '</div>';
		
		return myform;
	}
	
	addEntity(entity) {
		
		let dialog = bootbox.dialog({
            title: 'Ajout en cours '+entity['id_entity'],
            message: '<p><i class="fa fa-spin fa-spinner"></i> Ajout...</p><br>'

        });

        $.post('action/entity-action.php',
            {
                'action':'add',
				'id_book':entity['id_book'],
                'id_entity':entity['id_entity'],
                'nb_times_borrowed':entity['nb_times_borrowed'],
                'item_status_rate':entity['item_status_rate'],
				'available':entity['available']
            },function(data) {
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Ajouté avec succès');
                }
                else {
                    dialog.find('.bootbox-body').html('Erreur lors de l\'ajout');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });

		
	}
	
	editEntity(entity) {
		
		let dialog = bootbox.dialog({
            title: 'Modifcation en cours '+entity['id_entity'],
            message: '<p><i class="fa fa-spin fa-spinner"></i> Modification...</p><br>'

        });

        $.post('action/entity-action.php',
            {
                'action':'update',
                'id_entity':entity['id_entity'],
                'nb_times_borrowed':entity['nb_times_borrowed'],
                'item_status_rate':entity['item_status_rate'],
				'available':entity['available']
            },function(data) {
                if (data=='Success') {
                    dialog.find('.bootbox-body').html('Modifié avec succès');
                }
                else {
                    dialog.find('.bootbox-body').html('Echec lors de la modifcation');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });

		
	}
	
	deleteEntity(id_entity) {
		
		let dialog = bootbox.dialog({
            title: 'Suppression de l\'exemplaire '+id_entity,
            message: '<p><i class="fa fa-spin fa-spinner"></i> Suppression en cours...</p><br>'

        });

        $.post('action/entity-action.php',
            {
                'action':'delete',
                'id_entity':id_entity
            },function(data) {
            console.log(data);
                if (data=='Success') {
					$('#row_entity-' + id_entity).remove();
                    dialog.find('.bootbox-body').html('Supprimé avec succès');
                }
                else {
                    dialog.find('.bootbox-body').html('Echec lors de la suppression');
                }

                dialog.find('.bootbox-close-button').show();
            })
            .fail(function(data) {

            });
		
	}
	
	escapeHtml(text) {
		
	var map = {
    '&': '&amp;',
    '<': '&lt;',
    '>': '&gt;',
    '"': '&quot;',
    "'": '&#039;'
	};

  return text.replace(/[&<>"']/g, function(m) { return map[m]; });
  
  }
  
  
	
}