<?php

/**
Php class to represent a loan
**/

class loan {
	
	private $id;
	private $book; // book object concerned by the loan
	private $book_entity; // book_entity id concerned by the loan
	private $member; // member concerned by the loan
	private $start_date; // when the loan was created
	private $end_date; // when it is supposed to finish
	private $is_finished; // Is the loan finished and the entity available again ?

    /**
     * loan constructor.
     * @param $id
     * @param $book book_full
     * @param $book_entity
     * @param $member member
     * @param $start_date
     * @param $end_date
     */
    public function __construct($id, $book, $book_entity, $member, $start_date, $end_date,$is_finished)
    {
        $this->id = $id;
        $this->book = $book;
        $this->book_entity = $book_entity;
        $this->member = $member;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->is_finished = $is_finished;
    }


    public function getId(){
		return $this->id;
	}

	public function setId($id){
		$this->id = $id;
	}

    /**
     * @return book_full
     */
	public function getBook(){
		return $this->book;
	}

	public function setBook($book){
		$this->book = $book;
	}

	public function getBook_entity(){
		return $this->book_entity;
	}

	public function setBook_entity($book_entity){
		$this->book_entity = $book_entity;
	}

	public function getMember(){
		return $this->member;
	}

	public function setMember($member){
		$this->member = $member;
	}

	public function getStart_date(){
		return $this->start_date;
	}

	public function setStart_date($start_date){
		$this->start_date = $start_date;
	}

	public function getEnd_date(){
		return $this->end_date;
	}

	public function setEnd_date($end_date){
		$this->end_date = $end_date;
	}

    /*
    Translate the is_finished status in french
    */
    public function getIsFinished()
    {
        if ($this->is_finished=='1')
            return 'Oui';
        else
            return 'Non';
    }

    public function setIsFinished($is_finished)
    {
        $this->is_finished = $is_finished;
    }


	
}


?>
