<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
$pageTitle = 'Contact';
include_once('includes/header.php');
?>
<!-- header -->
<!--start-contact-->
		<div class="contact">
			<div class="container">
				<div class="contact-main">
					<h3>Contactez-Nous</h3>
					<div class="contact-top">
						<div class="col-md-4 contact-top-left">
							<div class="contact-top-one">
								<h4>ADDRESSE:</h4>
									<h6>The Company Name agi.
									<span>756 gt globel Place,</span>
										CD-Road,M 07 435.
									</h6>
							</div>
							<div class="contact-top-one">
								<h4>TELEPHONES:</h4>
									<p>Telephone: +1 234 567 9871
									<span>FAX: +1 234 567 9871</span>
									</p>
							</div>
							<div class="contact-top-one">
								<h4>E-MAIL:</h4>
								<p><a href="mailto:admin@lostarchives.fr">ADMIN@LOSTARCHIVES.FR</a></p>
							</div>
						</div>
						<div class="col-md-8 contact-top-right">
							<form>
							<input type="text" placeholder="Name" required="">
							<input type="text" placeholder="Email" required="">
							<input type="text" placeholder="Subject" required="">
							<textarea placeholder="Message" required=""></textarea>
							<div class="sub-button">
								<input type="submit" value="SEND">
							</div>
							</form>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="contact-bottom">
				<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3806.7201236595974!2d78.43322599999999!3d17.425214!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bcb90d4d27091db%3A0x6c57b46677eaa90e!2sBanjara+Harley-Davidson!5e0!3m2!1sen!2sin!4v1423718741402" frameborder="0" style="border:0"></iframe>
			</div>
			</div>
			
		</div>
		<!--End-contact-->

<!-- later -->
	<div class="later">
		<div class="container">
			<div class="col-md-6 later-right">
				<li><i class="ttt"></i></li>
				<li><h6>Proin enim velit, fermentum at malesuada in, ipsum... <span><a href="#">@graphicsfuel</a><span></h6></li>
			</div>
			<div class="clearfix"> </div>
		</div>
	</div>
<!-- later -->
<!-- footer -->
<?php
include_once('includes/footer.php');
?>
<!-- footer -->
</body>
</html>