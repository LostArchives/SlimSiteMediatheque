<?php

/**
Php file to handle actions related to the member table
**/

require_once ('../vendor/autoload.php');
require_once('../model/mediaClient.class.php');

$client = new GuzzleHttp\Client();

$action = '';
$id_member = '';

if (isset($_POST['action'])) {
    $action = $_POST['action']; // Action performed (add,edit,delete or get)
}

if (isset($_POST['id_member'])) {
    $id_member = $_POST['id_member'];
}

switch ($action) {

    case 'add':
		$member_pseudo = $_POST['member_pseudo'];
        $member_name = $_POST['member_name'];
        $member_avatar = $_POST['member_avatar'];
        $request = $client->post(mediaClient::URL_SERVICE.'members', array(
                'json' => array(
                    'pseudo' => $member_pseudo,
                    'name' => $member_name,
                    'avatar' => $member_avatar
                )
            )
        );
        $response = json_decode($request->getBody()->getContents(), true);
        if (isset($response['id'])) { // In slimservice api, every methods returns the object with its id when successful except DELETE
            echo 'Success';
        } else {
            echo 'Fail';
        }
        break;

    case 'update':
        $member_name = $_POST['member_name'];
        $member_avatar = $_POST['member_avatar'];
        $response = $client->put(mediaClient::URL_SERVICE.'members/' . $id_member, array(
                'json' => array(
                    'name' => $member_name,
                    'avatar' => $member_avatar
                )
            )
        );
        $response = json_decode($response->getBody()->getContents(), true);

        if (isset($response['id'])) { // In slimservice api, every methods returns the object with its id when successful except DELETE
            echo 'Success';
        } else {
            echo 'Fail';
        }
        break;

    case 'delete':
        $response = $client->delete(mediaClient::URL_SERVICE.'members/' . $id_member);
        $response = $response->getBody()->getContents();
        if ($response == '1') { // In slimservice API, DELETE requests return '1' when successful
            echo 'Success';
        } else {
            echo 'Fail' . ' ' . $response;
        }
        break;

    case 'get':
        
        $response = $client->get(mediaClient::URL_SERVICE.'members', array());
        $response = json_encode($response->getBody()->getContents(),true);
        echo $response;
        break;

    default:
        echo "I don't know what to do :p";
        break;

}


?>
