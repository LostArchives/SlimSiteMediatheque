<?php

/**
Php fiel to handle actions related to the table 'borrowed'
**/

require_once ('../vendor/autoload.php');
require_once('../model/mediaClient.class.php');

$client = new GuzzleHttp\Client();

$action = '';
$id_loan = '';
$id_entity = '';
$id_member = '';

if (isset($_POST['action'])) {
    $action = $_POST['action']; // Action performed (add,edit,delete)
}

if (isset($_POST['id_loan'])) {
    $id_loan = $_POST['id_loan'];
}

if (isset($_POST['id_entity'])) {
    $id_entity = $_POST['id_entity'];
}

if (isset($_POST['id_member'])) {
    $id_member = $_POST['id_member'];
}

switch ($action) {

    case 'add':
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        $finished = $_POST['finished'];
        $request = $client->post(mediaClient::URL_SERVICE.'loan', array(
                'json' => array(
					'id_member' => $id_member,
					'id_entity' => $id_entity,
					'start_date' => $start_date,
                    'end_date' => $end_date,
                    'finished' => $finished
                )
            )
        );
        $response = json_decode($request->getBody()->getContents(), true);
        if (isset($response['id_loan'])) { // In slimservice api, every methods returns the object with its id when successful except DELETE
            echo 'Success';
        } else {
            echo 'Fail';
        }
        break;

    case 'update':
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        $finished = $_POST['finished'];
        $request = $client->put(mediaClient::URL_SERVICE.'loan/'.$id_loan, array(
                'json' => array(
                    'start_date' => $start_date,
                    'end_date' => $end_date,
                    'finished' => $finished
                )
            )
        );
        $response = json_decode($request->getBody()->getContents(), true);
        if (isset($response['id_loan'])) { // In slimservice api, every methods returns the object with its id when successful except DELETE
            echo 'Success';
        } else {
            echo 'Fail';
        }
        break;

    case 'delete':
        $response = $client->delete(mediaClient::URL_SERVICE.'loan/'.$id_loan);
        $response = $response->getBody()->getContents();
        if ($response == '1') { // In slimservice API, DELETE requests return '1' when successful
            echo 'Success';
        } else {
            echo 'Fail'.' '.$response;
        }
        break;

   
    default:
        echo "I don't know what to do :p";
        break;

}


?>
