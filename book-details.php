<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<?php
require_once ('model/mediaClient.class.php');

$id_book = -1;
$book = null;

if (isset($_GET['id_book'])) {
    $id_book = $_GET['id_book'];
    $media_client = mediaClient::Instance();
    $book = $media_client->getBook($id_book);
    if ($book->getId() != $id_book) {
        header("Location: 404.php");
        exit();
    }

} else {
    header("Location: 404.php");
    exit();
}


?>

<?php

$pageTitle = "Détails du Livre";

include_once('includes/header.php');

?>

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip(); 
});
</script>

<!--
<div id="message">
    <div style="padding: 5px;">
        <div class="alert alert-success alert-dismissable">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
  <strong>Success!</strong> Indicates a successful or positive action.
</div>
    </div>
</div>
-->

<section>
<div class="container">
    <div class="about-main">
        <h3 style="color:yellow">Détails du livre</h3>
<div class="about-top">
					<div class="col-md-6">
						<img src="<?php echo $book->getImageUrl(); ?>" id="book_image_url-<?php echo $id_book; ?>" style="width:200px;float:right" />
					</div>
					<div class="col-md-6 about-top-right">
                        <h4 style="color:white;" id="book_title-<?php echo $id_book; ?>">Titre : <?php echo $book->getName(); ?></h4>
						<p style="color:white;" id="book_author-<?php echo $id_book; ?>">Auteur : <?php echo $book->getAuthor(); ?></p>
						<p style="color:white;" id="book_category-<?php echo $id_book; ?>">Catégorie : <?php echo $book->getCategory(); ?></p>
						<p style="color:white;">Exemplaires Total : <?php echo $book->getNb_entities_total(); ?></p>
						<p style="color:white;">Exemplaires disponibles : <?php echo $book->getNb_entities_available(); ?></p>
						<button style="margin-top:15px" type="button" class="btn btn-info book-action edit-book" id="<?php echo 'edit_book_details-' . $id_book; ?>"><i class="fa fa-pencil" aria-hidden="true"></i> Editer Informations</button>
                        <button style="margin-top:15px" type="button" class="btn btn-warning add_entity" id="add_entity-<?php echo $id_book; ?>" data-toggle="tooltip" title="Ajouter un exemplaire"><i class="fa fa-plus" aria-hidden="true"></i> Ajouter un exemplaire </button>
					</div>
					<div class="clearfix"></div>
					
</div>
    </div>
</div>
<div style="margin:50px;"></div>
<h1>Liste des Exemplaires</h1>
<div class="media-table">
  <div class="tbl-header">
    <table cellpadding="0" cellspacing="0" border="0">
      <thead>
        <tr>
          <th>N° Exemplaire</th>
          <th>Livre</th>
          <th>Titre</th>
          <th>Auteur</th>
          <th>Catégorie</th>
		  <th>Nombre de fois emprunté</th>
          <th>Etat de l'objet (0 - 10)</th>
		  <th>Disponible ?</th>
          <th>Actions</th>
        </tr>
      </thead>
    </table>
  </div>
    <?php
    $book_entities = $book->getBook_entities();
    if (count($book_entities)>0) {
    ?>
  <div class="tbl-content">
    <table cellpadding="0" cellspacing="0" border="0">
      <tbody>
      <?php foreach ($book_entities as $entity) { ?>
          <tr id="row_entity-<?php echo $entity->getId(); ?>">
          <td id="entity_id-<?php echo $entity->getId(); ?>">00<?php echo $entity->getId();  ?></td>
          <td id="entity_image-<?php echo $entity->getId(); ?>"><img src="<?php echo $book->getImageUrl(); ?>" style="width:65px" /></td>
          <td id="entity_name-<?php echo $entity->getId(); ?>"><?php echo $book->getName(); ?></td>
          <td id="entity_author-<?php echo $entity->getId(); ?>"><?php echo $book->getAuthor(); ?></td>
          <td id="entity_category-<?php echo $entity->getId(); ?>"><?php echo $book->getCategory(); ?></td>
          <td id="entity_timesBorrowed-<?php echo $entity->getId(); ?>"><?php echo $entity->getNbBorrowed(); ?></td>
          <td id="entity_statusRate-<?php echo $entity->getId(); ?>"><?php echo $entity->getItemStatusIcon('4x').' ('.$entity->getItemStatusRate().')'; ?></td>
		  <td id="entity_available-<?php echo $entity->getId(); ?>"><?php echo $entity->getIsAvailable(); ?></td>
		  <td>
		  <?php
		  $borrowButton = '';
		  if ($entity->getIsAvailable()=='Non') {
			  $borrowButton = 'disabled';
		  }
		  ?>
		  <button type="button" class="btn btn-primary add_loan" id="add_loan-<?php echo $entity->getId(); ?>" data-toggle="tooltip" title="Emprunter cet exemplaire" <?php echo $borrowButton; ?>><i class="fa fa-shopping-cart" aria-hidden="true"></i></button>
		  <button type="button" class="btn btn-warning edit_entity" id="edit_entity-<?php echo $entity->getId(); ?>" data-toggle="tooltip" title="Mettre à jour exemplaire"><i class="fa fa-pencil" aria-hidden="true"></i></button>
		  <button type="button" class="btn btn-danger delete_entity" id="delete_entity-<?php echo $entity->getId(); ?>" data-toggle="tooltip" title="Supprimer l'exemplaire"><i class="fa fa-times" aria-hidden="true"></i></button>
		  </td>
        </tr>
      <?php } ?>
      </tbody>
    </table>
  </div>
  </div>
    <?php }
    else { ?>
        <div class="main2">
            <div class="error-404 text-center">
                <h1>Aucun exemplaire existant
                <i class="fa fa-meh-o" aria-hidden="true"></i>
                </h1>
            </div>
        </div>
    <?php } ?>
</section>
<!-- banner -->
<!-- Here -->
	<div class="Here">
		<div class="workes">
			<h2><a href="portfolio.html">See All Our Works</a></h2>
		</div>
		<div class="container">
			<div class="col-md-3 here-1">
				<div class="here-left">
					<i class="bulb"></i>
				</div>
				<div class="here-right">
					<h5>Your 1st title Here</h5>
				</div>
					<div class="clearfix"> </div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim. </p>
			</div>
			<div class="col-md-3 here-1">
				<div class="here-left">
					<i class="lock"></i>
				</div>
				<div class="here-right">
					<h5>Your 2nd title Here</h5>
				</div>
					<div class="clearfix"> </div>
				<p>Nulla accumsan libero id quam facilisis nec luctus libero sollicitudin. Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. </p>
			</div>
			<div class="col-md-3 here-1">
				<div class="here-left">
					<i class="pen"></i>
				</div>
				<div class="here-right">
					<h5>Your 3rd title Here</h5>
				</div>
					<div class="clearfix"> </div>
				<p>Nullam tempus turpis at lorem posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit fermentum augue. Etiam eget magna vel ante mattis ultricies vitae ut nunc. </p>
			</div>
			<div class="col-md-3 here-1">
				<div class="here-left">
					<i class="adm"></i>
				</div>
				<div class="here-right">
					<h5>Your 4th title Here</h5>
				</div>
					<div class="clearfix"> </div>
				<p>Nam ac molestie ante. Pellentesque turpis lacus, vulputate vitae feugiat quis, dictum eget diam. Proin sapien libero, tempus et tempus ac, pulvinar quis nulla. </p>
			</div>
				<div class="clearfix"> </div>
		</div>
	</div>
<!-- Here -->
<!-- company -->
	<div class="company">
		<div class="container">
			<div class="col-md-4 company-1">
				<h3>Our Company</h3>
				<div class="our">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim. Nulla accumsan libero id quam facilisis nec luctus libero sollicitudin. </p>
				</div>
				<div class="our">
					<p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem posuere sodales. Donec nibh urna, auctor at eleifend eget, blandit fermentum augue.</p>
				</div>
				<div class="our">
					<p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem rutrum venenatis sed laoreet dui. </p>
				</div>
			</div>
			<div class="col-md-4 company-1">
				<h3>Our Testimonials</h3>
				<div class="our">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean molestie, lectus sed posuere consectetur, dolor ipsum tempus justo, quis ornare magna urna id enim.</p>
					<h6>Rafi, GraphicsFuel.com </h6>
				</div>
				<div class="our">
					<p>Sed ligula libero, feugiat sed gravida vel, commodo sit amet nibh. Nullam tempus turpis at lorem posuere sodales. Donec nibh urna, auctor at </p>
					<h6>Rafi, GraphicsFuel.com </h6>
				</div>
				<div class="our">
					<p>Proin enim velit, fermentum at malesuada in, porta vel ipsum. Pellentesque a erat sit amet lorem rutrum venenatis sed laoreet dui. </p>
					<h6>Rafi, GraphicsFuel.com </h6>
				</div>
			</div>
			<div class="col-md-4 company-1">
				<h3>From the blog</h3>
				<div class="sleek">
					<h5><a href="single.html">Sleek minimal website PSD template</a></h5>
					<h6>August 30, 2010, 9:32 am</h6>
				</div>
				<div class="sleek">
					<h5><a href="single.html">PSD ecommerce website template</a></h5>
					<h6>October 18, 2010, 5:51 am</h6>
				</div>
				<div class="sleek">
					<h5><a href="single.html">Sleeko- Download single page website design</a></h5>
					<h6>July 22, 2010, 7:19 pm</h6>
				</div>
				<div class="sleek">
					<h5><a href="single.html">Download web buttons in PSD & PNG (pack of 60)</a></h5>
					<h6>September 13, 2010, 2:15 am</h6>
				</div>
				<div class="sleek">
					<h5><a href="single.html">Flip clock countdown (PSD)</a></h5>
					<h6>August 12, 2011, 10:56 am</h6>
				</div>
			</div>
				<div class="clearfix"> </div>
		</div>
	</div>
<!-- company -->
<!-- later -->

<!-- later -->
<?php
include_once('includes/footer.php');
?>